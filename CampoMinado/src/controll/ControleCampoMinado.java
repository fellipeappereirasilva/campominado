package controll;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Fellipe
 */
public class ControleCampoMinado {
    
   private ArrayList bombas;
   private  Random r;
   private int quantidade;
   
   public ControleCampoMinado(ArrayList bomb, int quantidade){
       this.bombas = bomb;
       this.quantidade = quantidade;
       r = new Random();
       localBombas();
   }
    
   //Define os locais das bombas
   
   private void localBombas(){
       
       int numero,i,j,controle=0;
       boolean repetido;
       numero = r.nextInt(100);
        
       for(i = 0; i < quantidade; i++){
           System.out.println("numero "+numero);
        if(!bombas.isEmpty()){  
            repetido = true;
            while(repetido && controle < 50){
              repetido = false;
              numero = r.nextInt(100);
            for(j=0; j < bombas.size() && !repetido; j++){
                if(bombas.get(j).equals(numero) || numero == 0){
                    repetido = true;
                }
            }
            controle++;//evita loop infinito em caso de Random gerar sempre o mesmo valor
                System.out.println("controle "+controle);
            }  
            if(controle < 50)
              bombas.add(numero);
            
           controle = 0;
      
        }else
            bombas.add(numero);
         }
       
       
       
       
            System.out.println("\n\n\n");
             for(i = 0; i < bombas.size(); i++){
                 System.err.println("bomba "+bombas.get(i));
             }  
   }
    
   
   //define o valor que vai ser mostrado caso não aja bomba
   public int jButtonSuperiorEsquerdo(int cod){
       int quantBombas = 0,i,y,x;
       y = (cod + 1);
       x = (cod + 10);
       
       for(i = 0; i < bombas.size(); i++){
           
           if(bombas.get(i).equals(y) || bombas.get(i).equals(x) || bombas.get(i).equals(x+1)){
               quantBombas++;
           }  
       }
       
       return quantBombas;
   }
    
   //define o valor que vai ser mostrado caso não aja bomba
   public int jButtonInferiorEsquerdo(int cod){
       int quantBombas = 0,i,y,x;
       
       y = (cod - 10);
       x = (cod + 1);
       
       for(i = 0; i < bombas.size(); i++){
           
           if(bombas.get(i).equals(y) || bombas.get(i).equals(y + 1) || bombas.get(i).equals(x)){
               quantBombas++;
           }
           
       }
       
       return quantBombas;
   }
   
   
   //define o valor que vai ser mostrado caso não aja bomba
   public int jButtonInferiorDireito(int cod){
       int quantBombas = 0,i,y,x;
       
       y = (cod - 10);
       x = (cod - 1);
       
       for(i = 0; i < bombas.size(); i++){
           
           if(bombas.get(i).equals(y - 1) || bombas.get(i).equals(y) || bombas.get(i).equals(x))
               quantBombas++;
       }
        
       return quantBombas;
   }
   
   
   public int jButtonSuperiorDireito(int cod){
       int quantBombas = 0,i,y,x;
       y = (cod - 1);
       x = (cod + 10);
       
       for(i = 0; i < bombas.size(); i++){
           if(bombas.get(i).equals(y) || bombas.get(i).equals(x - 1) || bombas.get(i).equals(x))
               quantBombas++;
       }
        
       return quantBombas;
   }
   
   
   //define o valor que vai ser mostrado caso não aja bomba
   public int fileiraSuperior(int cod){
     
       int i,quantBombas = 0,x,y;
       y = cod;
       x = (cod + 10);
       
       for(i = 0; i < bombas.size(); i++){
           if(bombas.get(i).equals(y - 1) || bombas.get(i).equals(y + 1)){
               quantBombas++;
                }
           if(bombas.get(i).equals(x - 1) || bombas.get(i).equals(x) || bombas.get(i).equals(x + 1)){
               quantBombas++;
               
           }
       }
          
         return quantBombas;
    
   
}
   
   
   //define o valor que vai ser mostrado caso não aja bomba
    public int fileiraEsquerda(int cod){
        int quantBombas = 0,i,y,x;
        
        y = (cod - 10);
        x = (cod + 10);
        
        for(i = 0; i < bombas.size(); i++){
            if(bombas.get(i).equals(y) || bombas.get(i).equals(y + 1) || bombas.get(i).equals(cod + 1))
                quantBombas++;
            if(bombas.get(i).equals(x) || bombas.get(i).equals(x + 1))
                quantBombas++;
        }
         
        return quantBombas;
    }
   
   //define o valor que vai ser mostrado caso não aja bomba
   public int fileiraInferior(int cod){
       int quantBombas = 0,i,y,x;
       
       y = (cod -10);
       x = cod;
       
       for(i = 0; i < bombas.size(); i++){
       if(bombas.get(i).equals(y - 1) || bombas.get(i).equals(y) || bombas.get(i).equals(y + 1))
           quantBombas++;
       if(bombas.get(i).equals(x - 1) || bombas.get(i).equals(x + 1))
           quantBombas++;
       }
       
       return quantBombas;
   }
   
   //define o valor que vai ser mostrado caso não aja bomba
   public int fileiraDireita(int cod){
       int quantBombas = 0,i,y,x;
       
       y = (cod - 10);
       x = (cod + 10);
       
       for(i = 0; i < bombas.size(); i++){
           if(bombas.get(i).equals(y - 1) || bombas.get(i).equals(y))
               quantBombas++;
           if(bombas.get(i).equals(cod - 1) || bombas.get(i).equals(x - 1) || bombas.get(i).equals(x))
               quantBombas++;
       }
       
       return quantBombas;
       
   }
   
   //define o valor que vai ser mostrado caso não aja bomba
   public int jButtonsCentro(int cod){
       int quantBombas = 0,i,y,x;
       y = (cod - 10);
       x = (cod + 10);
             
       for(i = 0; i < bombas.size(); i++){
           
           if(bombas.get(i).equals(y - 1) || bombas.get(i).equals(y) || bombas.get(i).equals(y +1))
               quantBombas++;
           if(bombas.get(i).equals(cod - 1) || bombas.get(i).equals(cod + 1))
               quantBombas++;
           if(bombas.get(i).equals(x - 1) || bombas.get(i).equals(x) || bombas.get(i).equals(x + 1))
               quantBombas++;
           
       }
       
       return quantBombas;
}
   
}