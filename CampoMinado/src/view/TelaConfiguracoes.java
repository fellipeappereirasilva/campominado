package view;

/**
 *
 * @author Fellipe
 */
public class TelaConfiguracoes extends javax.swing.JFrame {

    
    public TelaConfiguracoes() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupNiveis = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jButtonRetorno = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jRadioButtonFacil = new javax.swing.JRadioButton();
        jRadioButtonMedio = new javax.swing.JRadioButton();
        jRadioButtonDificio = new javax.swing.JRadioButton();
        jCheckBoxLimiteDeTempo = new javax.swing.JCheckBox();
        jButtonConfirmar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(102, 102, 102));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Configurações", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11), new java.awt.Color(255, 255, 255))); // NOI18N

        jButtonRetorno.setBackground(new java.awt.Color(102, 102, 102));
        jButtonRetorno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/Back-icon.png"))); // NOI18N
        jButtonRetorno.setOpaque(false);
        jButtonRetorno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRetornoActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(102, 102, 102));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nível:");

        jRadioButtonFacil.setBackground(new java.awt.Color(102, 102, 102));
        buttonGroupNiveis.add(jRadioButtonFacil);
        jRadioButtonFacil.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonFacil.setForeground(new java.awt.Color(255, 255, 255));
        jRadioButtonFacil.setSelected(true);
        jRadioButtonFacil.setText("Fácil");
        jRadioButtonFacil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonFacilActionPerformed(evt);
            }
        });

        jRadioButtonMedio.setBackground(new java.awt.Color(102, 102, 102));
        buttonGroupNiveis.add(jRadioButtonMedio);
        jRadioButtonMedio.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonMedio.setForeground(new java.awt.Color(255, 255, 255));
        jRadioButtonMedio.setText("Médio");
        jRadioButtonMedio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMedioActionPerformed(evt);
            }
        });

        jRadioButtonDificio.setBackground(new java.awt.Color(102, 102, 102));
        buttonGroupNiveis.add(jRadioButtonDificio);
        jRadioButtonDificio.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonDificio.setForeground(new java.awt.Color(255, 255, 255));
        jRadioButtonDificio.setText("Difícil");
        jRadioButtonDificio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonDificioActionPerformed(evt);
            }
        });

        jCheckBoxLimiteDeTempo.setBackground(new java.awt.Color(102, 102, 102));
        jCheckBoxLimiteDeTempo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jCheckBoxLimiteDeTempo.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBoxLimiteDeTempo.setSelected(true);
        jCheckBoxLimiteDeTempo.setText("Limite de tempo");

        jButtonConfirmar.setBackground(new java.awt.Color(102, 102, 102));
        jButtonConfirmar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/Accept-icon.png"))); // NOI18N
        jButtonConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConfirmarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jCheckBoxLimiteDeTempo))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jRadioButtonFacil)
                                    .addComponent(jRadioButtonMedio)
                                    .addComponent(jRadioButtonDificio)))
                            .addComponent(jLabel1))
                        .addGap(0, 174, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButtonRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jButtonRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 32, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRadioButtonFacil)
                .addGap(3, 3, 3)
                .addComponent(jRadioButtonMedio)
                .addGap(6, 6, 6)
                .addComponent(jRadioButtonDificio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBoxLimiteDeTempo)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 340, 200));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButtonFacilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonFacilActionPerformed
       jRadioButtonFacil.setMnemonic(10);
    }//GEN-LAST:event_jRadioButtonFacilActionPerformed

    private void jRadioButtonMedioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMedioActionPerformed
       jRadioButtonMedio.setMnemonic(25);
    }//GEN-LAST:event_jRadioButtonMedioActionPerformed

    private void jRadioButtonDificioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonDificioActionPerformed
        jRadioButtonDificio.setMnemonic(35);
    }//GEN-LAST:event_jRadioButtonDificioActionPerformed

    private void jButtonConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConfirmarActionPerformed
        
        
        nivel = buttonGroupNiveis.getSelection().getMnemonic();
        if(jCheckBoxLimiteDeTempo.isSelected())
        limite = true;
        else
        limite = false;
       
        tCampo.recebeValores(nivel, limite);
            
        this.dispose();
    }//GEN-LAST:event_jButtonConfirmarActionPerformed

    private void jButtonRetornoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRetornoActionPerformed
             this.dispose();
    }//GEN-LAST:event_jButtonRetornoActionPerformed

   

    /**
     * @param args the command line arguments
     */
    private int nivel = 20;
    private boolean limite = false;
    private TelaCampoMinado tCampo;
    
    public void importaJframe(TelaCampoMinado tCampo){
        if(this.tCampo == null)
        this.tCampo = tCampo;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaConfiguracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaConfiguracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaConfiguracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaConfiguracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaConfiguracoes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupNiveis;
    private javax.swing.JButton jButtonConfirmar;
    private javax.swing.JButton jButtonRetorno;
    private javax.swing.JCheckBox jCheckBoxLimiteDeTempo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButtonDificio;
    private javax.swing.JRadioButton jRadioButtonFacil;
    private javax.swing.JRadioButton jRadioButtonMedio;
    // End of variables declaration//GEN-END:variables
}
