package view;

import java.util.ArrayList;
import controll.*;
import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;


/**
 *
 * @author Fellipe
 */
public class TelaCampoMinado extends javax.swing.JFrame {

    /**
     * Creates new form TelaCampoMinado
     */
    public TelaCampoMinado() {
        initComponents();
        if (nivel == 0) {
            nivel = 10;
        }
        controle = new ControleCampoMinado(bombas, nivel);
        quantBandeiras = nivel;
        jLabelQuantBandeiras.setText("" + nivel);
       
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton01 = new javax.swing.JButton();
        jButton02 = new javax.swing.JButton();
        jButton03 = new javax.swing.JButton();
        jButton04 = new javax.swing.JButton();
        jButton05 = new javax.swing.JButton();
        jButton06 = new javax.swing.JButton();
        jButton07 = new javax.swing.JButton();
        jButton08 = new javax.swing.JButton();
        jButton09 = new javax.swing.JButton();
        jButton010 = new javax.swing.JButton();
        jButton012 = new javax.swing.JButton();
        jButton011 = new javax.swing.JButton();
        jButton014 = new javax.swing.JButton();
        jButton013 = new javax.swing.JButton();
        jButton016 = new javax.swing.JButton();
        jButton015 = new javax.swing.JButton();
        jButton018 = new javax.swing.JButton();
        jButton017 = new javax.swing.JButton();
        jButton020 = new javax.swing.JButton();
        jButton019 = new javax.swing.JButton();
        jButton022 = new javax.swing.JButton();
        jButton021 = new javax.swing.JButton();
        jButton024 = new javax.swing.JButton();
        jButton023 = new javax.swing.JButton();
        jButton026 = new javax.swing.JButton();
        jButton025 = new javax.swing.JButton();
        jButton028 = new javax.swing.JButton();
        jButton027 = new javax.swing.JButton();
        jButton030 = new javax.swing.JButton();
        jButton029 = new javax.swing.JButton();
        jButton032 = new javax.swing.JButton();
        jButton031 = new javax.swing.JButton();
        jButton034 = new javax.swing.JButton();
        jButton033 = new javax.swing.JButton();
        jButton036 = new javax.swing.JButton();
        jButton035 = new javax.swing.JButton();
        jButton038 = new javax.swing.JButton();
        jButton037 = new javax.swing.JButton();
        jButton040 = new javax.swing.JButton();
        jButton039 = new javax.swing.JButton();
        jButton042 = new javax.swing.JButton();
        jButton041 = new javax.swing.JButton();
        jButton044 = new javax.swing.JButton();
        jButton043 = new javax.swing.JButton();
        jButton046 = new javax.swing.JButton();
        jButton045 = new javax.swing.JButton();
        jButton048 = new javax.swing.JButton();
        jButton047 = new javax.swing.JButton();
        jButton050 = new javax.swing.JButton();
        jButton049 = new javax.swing.JButton();
        jButton052 = new javax.swing.JButton();
        jButton051 = new javax.swing.JButton();
        jButton054 = new javax.swing.JButton();
        jButton053 = new javax.swing.JButton();
        jButton056 = new javax.swing.JButton();
        jButton055 = new javax.swing.JButton();
        jButton058 = new javax.swing.JButton();
        jButton057 = new javax.swing.JButton();
        jButton060 = new javax.swing.JButton();
        jButton059 = new javax.swing.JButton();
        jButton062 = new javax.swing.JButton();
        jButton061 = new javax.swing.JButton();
        jButton064 = new javax.swing.JButton();
        jButton063 = new javax.swing.JButton();
        jButton066 = new javax.swing.JButton();
        jButton065 = new javax.swing.JButton();
        jButton068 = new javax.swing.JButton();
        jButton067 = new javax.swing.JButton();
        jButton070 = new javax.swing.JButton();
        jButton069 = new javax.swing.JButton();
        jButton072 = new javax.swing.JButton();
        jButton071 = new javax.swing.JButton();
        jButton074 = new javax.swing.JButton();
        jButton073 = new javax.swing.JButton();
        jButton076 = new javax.swing.JButton();
        jButton075 = new javax.swing.JButton();
        jButton078 = new javax.swing.JButton();
        jButton077 = new javax.swing.JButton();
        jButton080 = new javax.swing.JButton();
        jButton079 = new javax.swing.JButton();
        jButton082 = new javax.swing.JButton();
        jButton081 = new javax.swing.JButton();
        jButton084 = new javax.swing.JButton();
        jButton083 = new javax.swing.JButton();
        jButton086 = new javax.swing.JButton();
        jButton085 = new javax.swing.JButton();
        jButton088 = new javax.swing.JButton();
        jButton087 = new javax.swing.JButton();
        jButton090 = new javax.swing.JButton();
        jButton089 = new javax.swing.JButton();
        jButton092 = new javax.swing.JButton();
        jButton091 = new javax.swing.JButton();
        jButton094 = new javax.swing.JButton();
        jButton093 = new javax.swing.JButton();
        jButton096 = new javax.swing.JButton();
        jButton095 = new javax.swing.JButton();
        jButton098 = new javax.swing.JButton();
        jButton097 = new javax.swing.JButton();
        jButton0100 = new javax.swing.JButton();
        jButton099 = new javax.swing.JButton();
        jLabelTextoTempo = new javax.swing.JLabel();
        jButtonConfiguracoes = new javax.swing.JButton();
        jLabelCronometro = new javax.swing.JLabel();
        jButtonReiniciar = new javax.swing.JButton();
        jButtonAtencao = new javax.swing.JButton();
        jButtonSair = new javax.swing.JButton();
        jLabelQuantBandeiras = new javax.swing.JLabel();
        jLabelResultado = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 102, 102));
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setResizable(false);
        setSize(new java.awt.Dimension(420, 535));

        jButton01.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton01.setForeground(new java.awt.Color(204, 0, 51));
        jButton01.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton01ActionPerformed(evt);
            }
        });

        jButton02.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton02.setForeground(new java.awt.Color(204, 0, 51));
        jButton02.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton02ActionPerformed(evt);
            }
        });

        jButton03.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton03.setForeground(new java.awt.Color(204, 0, 51));
        jButton03.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton03ActionPerformed(evt);
            }
        });

        jButton04.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton04.setForeground(new java.awt.Color(204, 0, 51));
        jButton04.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton04.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton04ActionPerformed(evt);
            }
        });

        jButton05.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton05.setForeground(new java.awt.Color(204, 0, 51));
        jButton05.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton05.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton05ActionPerformed(evt);
            }
        });

        jButton06.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton06.setForeground(new java.awt.Color(204, 0, 51));
        jButton06.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton06.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton06ActionPerformed(evt);
            }
        });

        jButton07.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton07.setForeground(new java.awt.Color(204, 0, 51));
        jButton07.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton07ActionPerformed(evt);
            }
        });

        jButton08.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton08.setForeground(new java.awt.Color(204, 0, 51));
        jButton08.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton08.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton08ActionPerformed(evt);
            }
        });

        jButton09.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton09.setForeground(new java.awt.Color(204, 0, 51));
        jButton09.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton09.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton09ActionPerformed(evt);
            }
        });

        jButton010.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton010.setForeground(new java.awt.Color(204, 0, 51));
        jButton010.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton010.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton010ActionPerformed(evt);
            }
        });

        jButton012.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton012.setForeground(new java.awt.Color(204, 0, 51));
        jButton012.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton012.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton012ActionPerformed(evt);
            }
        });

        jButton011.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton011.setForeground(new java.awt.Color(204, 0, 51));
        jButton011.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton011.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton011ActionPerformed(evt);
            }
        });

        jButton014.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton014.setForeground(new java.awt.Color(204, 0, 51));
        jButton014.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton014.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton014ActionPerformed(evt);
            }
        });

        jButton013.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton013.setForeground(new java.awt.Color(204, 0, 51));
        jButton013.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton013.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton013ActionPerformed(evt);
            }
        });

        jButton016.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton016.setForeground(new java.awt.Color(204, 0, 51));
        jButton016.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton016.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton016ActionPerformed(evt);
            }
        });

        jButton015.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton015.setForeground(new java.awt.Color(204, 0, 51));
        jButton015.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton015.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton015ActionPerformed(evt);
            }
        });

        jButton018.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton018.setForeground(new java.awt.Color(204, 0, 51));
        jButton018.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton018.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton018ActionPerformed(evt);
            }
        });

        jButton017.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton017.setForeground(new java.awt.Color(204, 0, 51));
        jButton017.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton017.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton017ActionPerformed(evt);
            }
        });

        jButton020.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton020.setForeground(new java.awt.Color(204, 0, 51));
        jButton020.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton020.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton020ActionPerformed(evt);
            }
        });

        jButton019.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton019.setForeground(new java.awt.Color(204, 0, 51));
        jButton019.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton019.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton019ActionPerformed(evt);
            }
        });

        jButton022.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton022.setForeground(new java.awt.Color(204, 0, 51));
        jButton022.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton022.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton022ActionPerformed(evt);
            }
        });

        jButton021.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton021.setForeground(new java.awt.Color(204, 0, 51));
        jButton021.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton021.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton021ActionPerformed(evt);
            }
        });

        jButton024.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton024.setForeground(new java.awt.Color(204, 0, 51));
        jButton024.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton024.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton024ActionPerformed(evt);
            }
        });

        jButton023.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton023.setForeground(new java.awt.Color(204, 0, 51));
        jButton023.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton023.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton023ActionPerformed(evt);
            }
        });

        jButton026.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton026.setForeground(new java.awt.Color(204, 0, 51));
        jButton026.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton026.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton026ActionPerformed(evt);
            }
        });

        jButton025.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton025.setForeground(new java.awt.Color(204, 0, 51));
        jButton025.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton025.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton025ActionPerformed(evt);
            }
        });

        jButton028.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton028.setForeground(new java.awt.Color(204, 0, 51));
        jButton028.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton028.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton028ActionPerformed(evt);
            }
        });

        jButton027.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton027.setForeground(new java.awt.Color(204, 0, 51));
        jButton027.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton027.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton027ActionPerformed(evt);
            }
        });

        jButton030.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton030.setForeground(new java.awt.Color(204, 0, 51));
        jButton030.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton030.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton030ActionPerformed(evt);
            }
        });

        jButton029.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton029.setForeground(new java.awt.Color(204, 0, 51));
        jButton029.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton029.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton029ActionPerformed(evt);
            }
        });

        jButton032.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton032.setForeground(new java.awt.Color(204, 0, 51));
        jButton032.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton032.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton032ActionPerformed(evt);
            }
        });

        jButton031.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton031.setForeground(new java.awt.Color(204, 0, 51));
        jButton031.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton031.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton031ActionPerformed(evt);
            }
        });

        jButton034.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton034.setForeground(new java.awt.Color(204, 0, 51));
        jButton034.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton034.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton034ActionPerformed(evt);
            }
        });

        jButton033.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton033.setForeground(new java.awt.Color(204, 0, 51));
        jButton033.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton033.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton033ActionPerformed(evt);
            }
        });

        jButton036.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton036.setForeground(new java.awt.Color(204, 0, 51));
        jButton036.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton036.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton036ActionPerformed(evt);
            }
        });

        jButton035.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton035.setForeground(new java.awt.Color(204, 0, 51));
        jButton035.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton035.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton035ActionPerformed(evt);
            }
        });

        jButton038.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton038.setForeground(new java.awt.Color(204, 0, 51));
        jButton038.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton038.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton038ActionPerformed(evt);
            }
        });

        jButton037.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton037.setForeground(new java.awt.Color(204, 0, 51));
        jButton037.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton037.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton037ActionPerformed(evt);
            }
        });

        jButton040.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton040.setForeground(new java.awt.Color(204, 0, 51));
        jButton040.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton040.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton040ActionPerformed(evt);
            }
        });

        jButton039.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton039.setForeground(new java.awt.Color(204, 0, 51));
        jButton039.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton039.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton039ActionPerformed(evt);
            }
        });

        jButton042.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton042.setForeground(new java.awt.Color(204, 0, 51));
        jButton042.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton042.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton042ActionPerformed(evt);
            }
        });

        jButton041.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton041.setForeground(new java.awt.Color(204, 0, 51));
        jButton041.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton041.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton041ActionPerformed(evt);
            }
        });

        jButton044.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton044.setForeground(new java.awt.Color(204, 0, 51));
        jButton044.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton044.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton044ActionPerformed(evt);
            }
        });

        jButton043.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton043.setForeground(new java.awt.Color(204, 0, 51));
        jButton043.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton043.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton043ActionPerformed(evt);
            }
        });

        jButton046.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton046.setForeground(new java.awt.Color(204, 0, 51));
        jButton046.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton046.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton046ActionPerformed(evt);
            }
        });

        jButton045.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton045.setForeground(new java.awt.Color(204, 0, 51));
        jButton045.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton045.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton045ActionPerformed(evt);
            }
        });

        jButton048.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton048.setForeground(new java.awt.Color(204, 0, 51));
        jButton048.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton048.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton048ActionPerformed(evt);
            }
        });

        jButton047.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton047.setForeground(new java.awt.Color(204, 0, 51));
        jButton047.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton047.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton047ActionPerformed(evt);
            }
        });

        jButton050.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton050.setForeground(new java.awt.Color(204, 0, 51));
        jButton050.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton050.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton050ActionPerformed(evt);
            }
        });

        jButton049.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton049.setForeground(new java.awt.Color(204, 0, 51));
        jButton049.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton049.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton049ActionPerformed(evt);
            }
        });

        jButton052.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton052.setForeground(new java.awt.Color(204, 0, 51));
        jButton052.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton052.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton052ActionPerformed(evt);
            }
        });

        jButton051.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton051.setForeground(new java.awt.Color(204, 0, 51));
        jButton051.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton051.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton051ActionPerformed(evt);
            }
        });

        jButton054.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton054.setForeground(new java.awt.Color(204, 0, 51));
        jButton054.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton054.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton054ActionPerformed(evt);
            }
        });

        jButton053.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton053.setForeground(new java.awt.Color(204, 0, 51));
        jButton053.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton053.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton053ActionPerformed(evt);
            }
        });

        jButton056.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton056.setForeground(new java.awt.Color(204, 0, 51));
        jButton056.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton056.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton056ActionPerformed(evt);
            }
        });

        jButton055.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton055.setForeground(new java.awt.Color(204, 0, 51));
        jButton055.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton055.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton055ActionPerformed(evt);
            }
        });

        jButton058.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton058.setForeground(new java.awt.Color(204, 0, 51));
        jButton058.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton058.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton058ActionPerformed(evt);
            }
        });

        jButton057.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton057.setForeground(new java.awt.Color(204, 0, 51));
        jButton057.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton057.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton057ActionPerformed(evt);
            }
        });

        jButton060.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton060.setForeground(new java.awt.Color(204, 0, 51));
        jButton060.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton060.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton060ActionPerformed(evt);
            }
        });

        jButton059.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton059.setForeground(new java.awt.Color(204, 0, 51));
        jButton059.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton059.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton059ActionPerformed(evt);
            }
        });

        jButton062.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton062.setForeground(new java.awt.Color(204, 0, 51));
        jButton062.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton062.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton062ActionPerformed(evt);
            }
        });

        jButton061.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton061.setForeground(new java.awt.Color(204, 0, 51));
        jButton061.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton061.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton061ActionPerformed(evt);
            }
        });

        jButton064.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton064.setForeground(new java.awt.Color(204, 0, 51));
        jButton064.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton064.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton064ActionPerformed(evt);
            }
        });

        jButton063.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton063.setForeground(new java.awt.Color(204, 0, 51));
        jButton063.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton063.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton063ActionPerformed(evt);
            }
        });

        jButton066.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton066.setForeground(new java.awt.Color(204, 0, 51));
        jButton066.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton066.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton066ActionPerformed(evt);
            }
        });

        jButton065.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton065.setForeground(new java.awt.Color(204, 0, 51));
        jButton065.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton065.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton065ActionPerformed(evt);
            }
        });

        jButton068.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton068.setForeground(new java.awt.Color(204, 0, 51));
        jButton068.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton068.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton068ActionPerformed(evt);
            }
        });

        jButton067.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton067.setForeground(new java.awt.Color(204, 0, 51));
        jButton067.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton067.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton067ActionPerformed(evt);
            }
        });

        jButton070.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton070.setForeground(new java.awt.Color(204, 0, 51));
        jButton070.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton070.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton070ActionPerformed(evt);
            }
        });

        jButton069.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton069.setForeground(new java.awt.Color(204, 0, 51));
        jButton069.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton069.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton069ActionPerformed(evt);
            }
        });

        jButton072.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton072.setForeground(new java.awt.Color(204, 0, 51));
        jButton072.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton072.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton072ActionPerformed(evt);
            }
        });

        jButton071.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton071.setForeground(new java.awt.Color(204, 0, 51));
        jButton071.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton071.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton071ActionPerformed(evt);
            }
        });

        jButton074.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton074.setForeground(new java.awt.Color(204, 0, 51));
        jButton074.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton074.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton074ActionPerformed(evt);
            }
        });

        jButton073.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton073.setForeground(new java.awt.Color(204, 0, 51));
        jButton073.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton073.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton073ActionPerformed(evt);
            }
        });

        jButton076.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton076.setForeground(new java.awt.Color(204, 0, 51));
        jButton076.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton076.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton076ActionPerformed(evt);
            }
        });

        jButton075.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton075.setForeground(new java.awt.Color(204, 0, 51));
        jButton075.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton075.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton075ActionPerformed(evt);
            }
        });

        jButton078.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton078.setForeground(new java.awt.Color(204, 0, 51));
        jButton078.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton078.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton078ActionPerformed(evt);
            }
        });

        jButton077.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton077.setForeground(new java.awt.Color(204, 0, 51));
        jButton077.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton077.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton077ActionPerformed(evt);
            }
        });

        jButton080.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton080.setForeground(new java.awt.Color(204, 0, 51));
        jButton080.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton080.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton080ActionPerformed(evt);
            }
        });

        jButton079.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton079.setForeground(new java.awt.Color(204, 0, 51));
        jButton079.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton079.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton079ActionPerformed(evt);
            }
        });

        jButton082.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton082.setForeground(new java.awt.Color(204, 0, 51));
        jButton082.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton082.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton082ActionPerformed(evt);
            }
        });

        jButton081.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton081.setForeground(new java.awt.Color(204, 0, 51));
        jButton081.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton081.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton081ActionPerformed(evt);
            }
        });

        jButton084.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton084.setForeground(new java.awt.Color(204, 0, 51));
        jButton084.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton084.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton084ActionPerformed(evt);
            }
        });

        jButton083.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton083.setForeground(new java.awt.Color(204, 0, 51));
        jButton083.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton083.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton083ActionPerformed(evt);
            }
        });

        jButton086.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton086.setForeground(new java.awt.Color(204, 0, 51));
        jButton086.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton086.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton086ActionPerformed(evt);
            }
        });

        jButton085.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton085.setForeground(new java.awt.Color(204, 0, 51));
        jButton085.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton085.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton085ActionPerformed(evt);
            }
        });

        jButton088.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton088.setForeground(new java.awt.Color(204, 0, 51));
        jButton088.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton088.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton088ActionPerformed(evt);
            }
        });

        jButton087.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton087.setForeground(new java.awt.Color(204, 0, 51));
        jButton087.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton087.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton087ActionPerformed(evt);
            }
        });

        jButton090.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton090.setForeground(new java.awt.Color(204, 0, 51));
        jButton090.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton090.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton090ActionPerformed(evt);
            }
        });

        jButton089.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton089.setForeground(new java.awt.Color(204, 0, 51));
        jButton089.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton089.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton089ActionPerformed(evt);
            }
        });

        jButton092.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton092.setForeground(new java.awt.Color(204, 0, 51));
        jButton092.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton092.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton092ActionPerformed(evt);
            }
        });

        jButton091.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton091.setForeground(new java.awt.Color(204, 0, 51));
        jButton091.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton091.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton091ActionPerformed(evt);
            }
        });

        jButton094.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton094.setForeground(new java.awt.Color(204, 0, 51));
        jButton094.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton094.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton094ActionPerformed(evt);
            }
        });

        jButton093.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton093.setForeground(new java.awt.Color(204, 0, 51));
        jButton093.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton093.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton093ActionPerformed(evt);
            }
        });

        jButton096.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton096.setForeground(new java.awt.Color(204, 0, 51));
        jButton096.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton096.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton096ActionPerformed(evt);
            }
        });

        jButton095.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton095.setForeground(new java.awt.Color(204, 0, 51));
        jButton095.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton095.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton095ActionPerformed(evt);
            }
        });

        jButton098.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton098.setForeground(new java.awt.Color(204, 0, 51));
        jButton098.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton098.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton098ActionPerformed(evt);
            }
        });

        jButton097.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton097.setForeground(new java.awt.Color(204, 0, 51));
        jButton097.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton097.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton097ActionPerformed(evt);
            }
        });

        jButton0100.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton0100.setForeground(new java.awt.Color(204, 0, 51));
        jButton0100.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton0100.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton0100ActionPerformed(evt);
            }
        });

        jButton099.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jButton099.setForeground(new java.awt.Color(204, 0, 51));
        jButton099.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jButton099.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton099ActionPerformed(evt);
            }
        });

        jLabelTextoTempo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelTextoTempo.setText("Tempo:");

        jButtonConfiguracoes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/conf.png"))); // NOI18N
        jButtonConfiguracoes.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jButtonConfiguracoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConfiguracoesActionPerformed(evt);
            }
        });

        jLabelCronometro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCronometro.setText("00 : 00");

        jButtonReiniciar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButtonReiniciar.setText("Reiniciar");
        jButtonReiniciar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonReiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReiniciarActionPerformed(evt);
            }
        });

        jButtonAtencao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/bomb-icon32px.png"))); // NOI18N
        jButtonAtencao.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonAtencao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAtencaoActionPerformed(evt);
            }
        });

        jButtonSair.setText("Sair");
        jButtonSair.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButtonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSairActionPerformed(evt);
            }
        });

        jLabelQuantBandeiras.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelQuantBandeiras.setText("10");
        jLabelQuantBandeiras.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jLabelQuantBandeiras.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelResultado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelResultado.setForeground(new java.awt.Color(255, 0, 0));

        jLabel1.setText("ADS_04");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonSair)
                        .addGap(157, 157, 157)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonConfiguracoes)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabelResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonReiniciar)
                                .addGap(43, 43, 43)
                                .addComponent(jLabelTextoTempo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelCronometro, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(17, 17, 17)
                        .addComponent(jLabelQuantBandeiras, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jButtonAtencao))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton01, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(jButton02, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton03, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton04, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton05, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton06, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton07, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton08, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton09, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton010, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton011, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton012, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton013, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton014, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton015, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton016, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton017, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton018, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton019, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton020, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton021, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton022, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton023, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton024, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton025, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton026, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton027, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton028, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton029, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton030, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton031, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton032, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton033, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton034, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton035, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton036, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton037, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton038, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton039, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton040, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton041, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton042, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton043, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton044, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton045, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton046, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton047, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton048, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton049, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton050, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton051, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton052, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton053, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton054, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton055, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton056, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton057, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton058, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton059, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton060, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton061, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton062, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton063, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton064, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton065, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton066, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton067, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton068, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton069, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton070, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton071, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton072, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton073, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton074, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton075, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton076, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton077, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton078, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton079, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton080, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton081, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton082, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton083, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton084, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton085, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton086, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton087, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton088, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton089, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton090, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton091, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton092, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton093, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton094, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton095, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton096, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton097, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton098, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton099, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton0100, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jButtonSair))
                    .addComponent(jLabel1))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jButtonConfiguracoes))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonReiniciar)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jLabelTextoTempo))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabelCronometro))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jLabelQuantBandeiras, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButtonAtencao, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton01, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton02, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton03, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton04, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton05, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton06, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton07, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton08, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton09, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton010, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton011, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton012, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton013, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton014, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton015, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton016, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton017, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton018, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton019, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton020, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton021, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton022, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton023, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton024, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton025, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton026, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton027, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton028, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton029, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton030, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton031, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton032, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton033, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton034, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton035, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton036, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton037, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton038, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton039, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton040, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton041, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton042, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton043, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton044, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton045, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton046, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton047, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton048, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton049, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton050, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton051, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton052, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton053, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton054, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton055, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton056, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton057, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton058, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton059, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton060, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton061, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton062, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton063, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton064, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton065, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton066, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton067, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton068, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton069, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton070, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton071, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton072, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton073, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton074, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton075, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton076, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton077, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton078, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton079, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton080, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton081, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton082, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton083, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton084, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton085, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton086, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton087, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton088, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton089, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton090, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton091, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton092, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton093, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton094, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton095, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton096, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton097, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton098, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton099, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton0100, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    private void jButton01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton01ActionPerformed
        setButtons(jButton01, 1);
    }//GEN-LAST:event_jButton01ActionPerformed

    private void jButton027ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton027ActionPerformed
        setButtons(jButton027, 27);
    }//GEN-LAST:event_jButton027ActionPerformed

    private void jButton029ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton029ActionPerformed
        setButtons(jButton029, 29);
    }//GEN-LAST:event_jButton029ActionPerformed

    private void jButton037ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton037ActionPerformed
        setButtons(jButton037, 37);
    }//GEN-LAST:event_jButton037ActionPerformed

    private void jButton042ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton042ActionPerformed
        setButtons(jButton042, 42);
    }//GEN-LAST:event_jButton042ActionPerformed

    private void jButton02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton02ActionPerformed
        setButtons(jButton02, 2);
    }//GEN-LAST:event_jButton02ActionPerformed

    private void jButton03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton03ActionPerformed
        setButtons(jButton03, 3);
    }//GEN-LAST:event_jButton03ActionPerformed

    private void jButton04ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton04ActionPerformed
        setButtons(jButton04, 4);
    }//GEN-LAST:event_jButton04ActionPerformed

    private void jButton05ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton05ActionPerformed
        setButtons(jButton05, 5);
    }//GEN-LAST:event_jButton05ActionPerformed

    private void jButton06ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton06ActionPerformed
        setButtons(jButton06, 6);
        /*
        }          }//GEN-LAST:event_jButton06ActionPerformed
*/
    }
    private void jButton07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton07ActionPerformed
        setButtons(jButton07, 7);
        /* 
        }          }//GEN-LAST:event_jButton07ActionPerformed
*/
    }
    private void jButton08ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton08ActionPerformed

        setButtons(jButton08, 8);
        /*
        }          }//GEN-LAST:event_jButton08ActionPerformed
*/
    }
    private void jButton09ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton09ActionPerformed
        setButtons(jButton09, 9);
    }//GEN-LAST:event_jButton09ActionPerformed

    private void jButton010ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton010ActionPerformed
        setButtons(jButton010, 10);
    }//GEN-LAST:event_jButton010ActionPerformed

    private void jButton011ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton011ActionPerformed
        setButtons(jButton011, 11);
    }//GEN-LAST:event_jButton011ActionPerformed

    private void jButton012ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton012ActionPerformed
        setButtons(jButton012, 12);
    }//GEN-LAST:event_jButton012ActionPerformed

    private void jButton013ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton013ActionPerformed
        setButtons(jButton013, 13);
    }//GEN-LAST:event_jButton013ActionPerformed

    private void jButton014ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton014ActionPerformed
        setButtons(jButton014, 14);
    }//GEN-LAST:event_jButton014ActionPerformed

    private void jButton015ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton015ActionPerformed
        setButtons(jButton015, 15);
    }//GEN-LAST:event_jButton015ActionPerformed

    private void jButton016ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton016ActionPerformed
        setButtons(jButton016, 16);
    }//GEN-LAST:event_jButton016ActionPerformed

    private void jButton017ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton017ActionPerformed
        setButtons(jButton017, 17);
    }//GEN-LAST:event_jButton017ActionPerformed

    private void jButton018ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton018ActionPerformed
        setButtons(jButton018, 18);
    }//GEN-LAST:event_jButton018ActionPerformed

    private void jButton019ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton019ActionPerformed
        setButtons(jButton019, 19);
    }//GEN-LAST:event_jButton019ActionPerformed

    private void jButton020ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton020ActionPerformed
        setButtons(jButton020, 20);
    }//GEN-LAST:event_jButton020ActionPerformed

    private void jButton021ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton021ActionPerformed
        setButtons(jButton021, 21);
    }//GEN-LAST:event_jButton021ActionPerformed

    private void jButton022ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton022ActionPerformed
        setButtons(jButton022, 22);
    }//GEN-LAST:event_jButton022ActionPerformed

    private void jButton023ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton023ActionPerformed
        setButtons(jButton023, 23);
    }//GEN-LAST:event_jButton023ActionPerformed

    private void jButton024ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton024ActionPerformed
        setButtons(jButton024, 24);
    }//GEN-LAST:event_jButton024ActionPerformed

    private void jButton025ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton025ActionPerformed
        setButtons(jButton025, 25);
    }//GEN-LAST:event_jButton025ActionPerformed

    private void jButton026ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton026ActionPerformed
        setButtons(jButton026, 26);
    }//GEN-LAST:event_jButton026ActionPerformed

    private void jButton028ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton028ActionPerformed
        setButtons(jButton028, 28);
    }//GEN-LAST:event_jButton028ActionPerformed

    private void jButton030ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton030ActionPerformed
        setButtons(jButton030, 30);
    }//GEN-LAST:event_jButton030ActionPerformed

    private void jButton031ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton031ActionPerformed
        setButtons(jButton031, 31);
    }//GEN-LAST:event_jButton031ActionPerformed

    private void jButton032ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton032ActionPerformed
        setButtons(jButton032, 32);
    }//GEN-LAST:event_jButton032ActionPerformed

    private void jButton033ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton033ActionPerformed
        setButtons(jButton033, 33);
    }//GEN-LAST:event_jButton033ActionPerformed

    private void jButton034ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton034ActionPerformed
        setButtons(jButton034, 34);
    }//GEN-LAST:event_jButton034ActionPerformed

    private void jButton035ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton035ActionPerformed
        setButtons(jButton035, 35);
    }//GEN-LAST:event_jButton035ActionPerformed

    private void jButton036ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton036ActionPerformed
        setButtons(jButton036, 36);
    }//GEN-LAST:event_jButton036ActionPerformed

    private void jButton038ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton038ActionPerformed
        setButtons(jButton038, 38);
    }//GEN-LAST:event_jButton038ActionPerformed

    private void jButton039ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton039ActionPerformed
        setButtons(jButton039, 39);
    }//GEN-LAST:event_jButton039ActionPerformed

    private void jButton040ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton040ActionPerformed
        setButtons(jButton040, 40);
    }//GEN-LAST:event_jButton040ActionPerformed

    private void jButton041ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton041ActionPerformed
        setButtons(jButton041, 41);
    }//GEN-LAST:event_jButton041ActionPerformed

    private void jButton043ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton043ActionPerformed
        setButtons(jButton043, 43);
    }//GEN-LAST:event_jButton043ActionPerformed

    private void jButton044ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton044ActionPerformed
        setButtons(jButton044, 44);
    }//GEN-LAST:event_jButton044ActionPerformed

    private void jButton045ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton045ActionPerformed
        setButtons(jButton045, 45);
    }//GEN-LAST:event_jButton045ActionPerformed

    private void jButton046ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton046ActionPerformed
        setButtons(jButton046, 46);
    }//GEN-LAST:event_jButton046ActionPerformed

    private void jButton047ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton047ActionPerformed
        setButtons(jButton047, 47);
    }//GEN-LAST:event_jButton047ActionPerformed

    private void jButton048ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton048ActionPerformed
        setButtons(jButton048, 48);
    }//GEN-LAST:event_jButton048ActionPerformed

    private void jButton049ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton049ActionPerformed
        setButtons(jButton049, 49);
    }//GEN-LAST:event_jButton049ActionPerformed

    private void jButton050ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton050ActionPerformed
        setButtons(jButton050, 50);
    }//GEN-LAST:event_jButton050ActionPerformed

    private void jButton051ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton051ActionPerformed
        setButtons(jButton051, 51);
    }//GEN-LAST:event_jButton051ActionPerformed

    private void jButton052ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton052ActionPerformed
        setButtons(jButton052, 52);
    }//GEN-LAST:event_jButton052ActionPerformed

    private void jButton053ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton053ActionPerformed
        setButtons(jButton053, 53);
    }//GEN-LAST:event_jButton053ActionPerformed

    private void jButton054ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton054ActionPerformed
        setButtons(jButton054, 54);
    }//GEN-LAST:event_jButton054ActionPerformed

    private void jButton055ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton055ActionPerformed
        setButtons(jButton055, 55);
    }//GEN-LAST:event_jButton055ActionPerformed

    private void jButton056ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton056ActionPerformed
        setButtons(jButton056, 56);
    }//GEN-LAST:event_jButton056ActionPerformed

    private void jButton057ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton057ActionPerformed
        setButtons(jButton057, 57);
    }//GEN-LAST:event_jButton057ActionPerformed

    private void jButton058ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton058ActionPerformed
        setButtons(jButton058, 58);
    }//GEN-LAST:event_jButton058ActionPerformed

    private void jButton059ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton059ActionPerformed
        setButtons(jButton059, 59);
    }//GEN-LAST:event_jButton059ActionPerformed

    private void jButton060ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton060ActionPerformed
        setButtons(jButton060, 60);
    }//GEN-LAST:event_jButton060ActionPerformed

    private void jButton061ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton061ActionPerformed
        setButtons(jButton061, 61);
    }//GEN-LAST:event_jButton061ActionPerformed

    private void jButton062ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton062ActionPerformed
        setButtons(jButton062, 62);
    }//GEN-LAST:event_jButton062ActionPerformed

    private void jButton063ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton063ActionPerformed
        setButtons(jButton063, 63);
    }//GEN-LAST:event_jButton063ActionPerformed

    private void jButton064ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton064ActionPerformed
        setButtons(jButton064, 64);
    }//GEN-LAST:event_jButton064ActionPerformed

    private void jButton065ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton065ActionPerformed
        setButtons(jButton065, 65);
    }//GEN-LAST:event_jButton065ActionPerformed

    private void jButton066ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton066ActionPerformed
        setButtons(jButton066, 66);
    }//GEN-LAST:event_jButton066ActionPerformed

    private void jButton067ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton067ActionPerformed
        setButtons(jButton067, 67);
    }//GEN-LAST:event_jButton067ActionPerformed

    private void jButton068ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton068ActionPerformed
        setButtons(jButton068, 68);
    }//GEN-LAST:event_jButton068ActionPerformed

    private void jButton069ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton069ActionPerformed
        setButtons(jButton069, 69);
    }//GEN-LAST:event_jButton069ActionPerformed

    private void jButton070ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton070ActionPerformed
        setButtons(jButton070, 70);
    }//GEN-LAST:event_jButton070ActionPerformed

    private void jButton071ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton071ActionPerformed
        setButtons(jButton071, 71);
    }//GEN-LAST:event_jButton071ActionPerformed

    private void jButton072ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton072ActionPerformed
        setButtons(jButton072, 72);
    }//GEN-LAST:event_jButton072ActionPerformed

    private void jButton073ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton073ActionPerformed
        setButtons(jButton073, 73);
    }//GEN-LAST:event_jButton073ActionPerformed

    private void jButton074ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton074ActionPerformed
        setButtons(jButton074, 74);
    }//GEN-LAST:event_jButton074ActionPerformed

    private void jButton075ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton075ActionPerformed
        setButtons(jButton075, 75);
    }//GEN-LAST:event_jButton075ActionPerformed

    private void jButton076ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton076ActionPerformed
        setButtons(jButton076, 76);
    }//GEN-LAST:event_jButton076ActionPerformed

    private void jButton077ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton077ActionPerformed
        setButtons(jButton077, 77);
    }//GEN-LAST:event_jButton077ActionPerformed

    private void jButton078ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton078ActionPerformed
        setButtons(jButton078, 78);
    }//GEN-LAST:event_jButton078ActionPerformed

    private void jButton079ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton079ActionPerformed
        setButtons(jButton079, 79);
    }//GEN-LAST:event_jButton079ActionPerformed

    private void jButton080ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton080ActionPerformed
        setButtons(jButton080, 80);
    }//GEN-LAST:event_jButton080ActionPerformed

    private void jButton081ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton081ActionPerformed
        setButtons(jButton081, 81);
    }//GEN-LAST:event_jButton081ActionPerformed

    private void jButton082ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton082ActionPerformed
        setButtons(jButton082, 82);
    }//GEN-LAST:event_jButton082ActionPerformed

    private void jButton083ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton083ActionPerformed
        setButtons(jButton083, 83);
    }//GEN-LAST:event_jButton083ActionPerformed

    private void jButton084ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton084ActionPerformed
        setButtons(jButton084, 84);
    }//GEN-LAST:event_jButton084ActionPerformed

    private void jButton085ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton085ActionPerformed
        setButtons(jButton085, 85);
    }//GEN-LAST:event_jButton085ActionPerformed

    private void jButton086ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton086ActionPerformed
        setButtons(jButton086, 86);
    }//GEN-LAST:event_jButton086ActionPerformed

    private void jButton087ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton087ActionPerformed
        setButtons(jButton087, 87);
    }//GEN-LAST:event_jButton087ActionPerformed

    private void jButton088ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton088ActionPerformed
        setButtons(jButton088, 88);
    }//GEN-LAST:event_jButton088ActionPerformed

    private void jButton089ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton089ActionPerformed
        setButtons(jButton089, 89);
    }//GEN-LAST:event_jButton089ActionPerformed

    private void jButton090ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton090ActionPerformed
        setButtons(jButton090, 90);
    }//GEN-LAST:event_jButton090ActionPerformed

    private void jButton091ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton091ActionPerformed
        setButtons(jButton091, 91);
    }//GEN-LAST:event_jButton091ActionPerformed

    private void jButton092ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton092ActionPerformed
        setButtons(jButton092, 92);
    }//GEN-LAST:event_jButton092ActionPerformed

    private void jButton093ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton093ActionPerformed
        setButtons(jButton093, 93);
    }//GEN-LAST:event_jButton093ActionPerformed

    private void jButton094ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton094ActionPerformed
        setButtons(jButton094, 94);
    }//GEN-LAST:event_jButton094ActionPerformed

    private void jButton095ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton095ActionPerformed
        setButtons(jButton095, 95);
    }//GEN-LAST:event_jButton095ActionPerformed

    private void jButton096ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton096ActionPerformed
        setButtons(jButton096, 96);
    }//GEN-LAST:event_jButton096ActionPerformed

    private void jButton097ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton097ActionPerformed
        setButtons(jButton097, 97);
    }//GEN-LAST:event_jButton097ActionPerformed

    private void jButton098ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton098ActionPerformed
        setButtons(jButton098, 98);
    }//GEN-LAST:event_jButton098ActionPerformed

    private void jButton099ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton099ActionPerformed
        setButtons(jButton099, 99);
    }//GEN-LAST:event_jButton099ActionPerformed

    private void jButton0100ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton0100ActionPerformed
        setButtons(jButton0100, 100);
    }//GEN-LAST:event_jButton0100ActionPerformed

    private void jButtonReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReiniciarActionPerformed
        int i = 0;

        for (i = 0; i < codigos.size(); i++) {

            if (!codigos.isEmpty()) {
                i = 0;
            }
            codigos.remove(i);

        }
        if (!codigos.isEmpty()) {
            codigos.remove(0);

        }

        for (i = 0; i < bombas.size(); i++) {

            if (!bombas.isEmpty()) {
                i = 0;
            }
            bombas.remove(i);

        }
        if (!bombas.isEmpty()) {
            bombas.remove(0);
        }

        for (i = 0; i < botao.size() && !botao.isEmpty(); i++) {

            if (!botao.isEmpty()) {
                i = 0;
            }
            botao.remove(i);

        }
        if (!botao.isEmpty()) {
            botao.remove(0);
             
        }
        
         for (i = 0; i < codBandeiras.size() && !codBandeiras.isEmpty(); i++) {

            if (!codBandeiras.isEmpty()) {
                i = 0;
            }
            codBandeiras.remove(i);

        }
        if (!codBandeiras.isEmpty()) {//garante que os arrays atão totalmente limpo
            codBandeiras.remove(0);
        }
        
  
        fimDeJogo = false;
        inicio = false;
        fimTempo = false;
        ganhou = false;
        this.dispose();
        new TelaCampoMinado().setVisible(true);

    }//GEN-LAST:event_jButtonReiniciarActionPerformed

    private void jButtonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonSairActionPerformed

    private void jButtonConfiguracoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConfiguracoesActionPerformed
        telaConf.importaJframe(this);
        telaConf.setVisible(true);
    }//GEN-LAST:event_jButtonConfiguracoesActionPerformed

    private void jButtonAtencaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAtencaoActionPerformed
        if (bandeira) {
            bandeira = false;
            jButtonAtencao.setIcon(abrirBomba);
        } else {
            bandeira = true;
            jButtonAtencao.setIcon(atencao);
        }
    }//GEN-LAST:event_jButtonAtencaoActionPerformed

    /**
     * @param args the command line arguments
     */
    TelaConfiguracoes telaConf = new TelaConfiguracoes();
    private static int nivel = 10, quantBandeiras = 10;
    private static boolean limiteTempo = false, fimTempo = false, ganhou = false;
    private static ArrayList bombas;
    private static ImageIcon icone;
    private static ImageIcon atencao;
    private static ImageIcon abrirBomba;
    private static ArrayList codigos;
    private static ArrayList botao;
    private static ArrayList codBandeiras;
    private static ControleCampoMinado controle;
    private boolean fimDeJogo = false, inicio = false;
    private boolean bandeira = false;

    public void recebeValores(int nivel, boolean limiteTempo) {
        this.nivel = nivel;
        this.limiteTempo = limiteTempo;
       }

    public static void main(String args[]) {

        botao = new ArrayList();
        bombas = new ArrayList();
        codigos = new ArrayList();
        codBandeiras = new ArrayList();

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaCampoMinado().setVisible(true);
                icone = new ImageIcon(getClass().getResource("/view/Bomb-icon.png"));
                atencao = new ImageIcon(getClass().getResource("/view/bandeira.png"));
                abrirBomba = new ImageIcon(getClass().getResource("/view/bomb-icon32px.png"));

            }
        });

    }

    private Runnable cronometro = new Runnable() {
        @Override
        public void run() {
            int j = 0, i;
            if (!limiteTempo) {
                jLabelCronometro.setBackground(new java.awt.Color(240, 240, 240));
                jLabelCronometro.setForeground(java.awt.Color.black);
                for (i = 1; i <= 60 && !fimDeJogo && !ganhou; i++) {
                    try {
                        Thread.sleep(1000);
                        if (i == 60) {
                            j++;
                            i = 1;
                        }
                        jLabelCronometro.setText("0" + j + " : " + i);

                    } catch (InterruptedException ex) {
                        Logger.getLogger(TelaCampoMinado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
              
            } else {
                j = 9;
                jLabelCronometro.setBackground(new java.awt.Color(0, 0, 0));
                jLabelCronometro.setForeground(java.awt.Color.red);

                for (i = 59; i >= 0 && j >= 0 && !fimDeJogo && !ganhou; i--) {
                    try {
                        Thread.sleep(1000);

                        jLabelCronometro.setText("0" + j + " : " + i);

                        if (i == 1) {
                            j--;
                            i = 59;
                        }

                    } catch (InterruptedException ex) {
                        Logger.getLogger(TelaCampoMinado.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                if (j < 0) {
                    jLabelCronometro.setText("0" + "0" + " : " + "0 0");
                    jLabelResultado.setText(" SEU TEMPO ACABOU! ");
                    fimTempo = true;
                }
                fimDeJogo = true;
               

            }
        }
    };

    private int n = 0, k = 1;

    private Runnable abrir = new Runnable() {

        @Override
        public void run() {
            boolean aberto = false;
            JButton auxiliar;

            for (int x = 1; x < botao.size(); x++) {
                aberto = false;
                for (int j = 0; j < codigos.size() && !aberto; j++) {
                    if (codigos.get(j).equals(x)) {
                        aberto = true;
                    }
                }
                if (!aberto) {
                    auxiliar = (JButton) botao.get(x);

                    for (int i = 0; i < bombas.size(); i++) {
                        if (bombas.get(i).equals(x) &&  !ganhou) {
                            auxiliar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
                            auxiliar.setBackground(Color.lightGray);
                            auxiliar.setIcon(icone);

                        } else {
                            setaJbuttons(x);
                        }
                    }
                }

            }

        }
    };

    private static void setaJbuttons(int cod) {
        int i, redorBombas, posicao;
        JButton auxiliar;

        for (i = 0; i < bombas.size(); i++) {
            if (bombas.get(i).equals(cod)) {
                return;
            }
        }

        i = 0;
        while (i < codigos.size()) {
            if (codigos.get(i).equals(cod)) {
                return;
            }
            i++;
        }
        
        codigos.add(cod);
        auxiliar = (JButton) botao.get(cod);
        if(!ganhou){
        for(i = 0; i < codBandeiras.size(); i++){
            if(codBandeiras.get(i).equals(cod)){
             auxiliar.setIcon(null);
             codBandeiras.remove(i);
              }
        }
        }
        auxiliar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
       //   auxiliar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        posicao = 9;

        if (cod == 1) {
            posicao = 1;
        }

        for (i = 11; i <= 81; i = (i + 10)) {
            if (cod == i) {
                posicao = 2;
            }
        }

        if (cod == 91) {
            posicao = 3;
        }

        if (cod > 91 && cod < 100) {
            posicao = 4;
        }

        if (cod == 100) {
            posicao = 5;
        }

        for (i = 20; i <= 90; i = (i + 10)) {
            if (cod == i) {
                posicao = 6;
            }

        }

        if (cod == 10) {
            posicao = 7;
        }

        if (cod > 1 && cod < 10) {
            posicao = 8;
        }

        auxiliar.setBackground(Color.lightGray);
        switch (posicao) {
            case 1:
                redorBombas = controle.jButtonSuperiorEsquerdo(cod);
                break;
            case 2:
                redorBombas = controle.fileiraEsquerda(cod);
                break;
            case 3:
                redorBombas = controle.jButtonInferiorEsquerdo(cod);
                break;
            case 4:
                redorBombas = controle.fileiraInferior(cod);
                break;
            case 5:
                redorBombas = controle.jButtonInferiorDireito(cod);
                break;
            case 6:
                redorBombas = controle.fileiraDireita(cod);
                break;
            case 7:
                redorBombas = controle.jButtonSuperiorDireito(cod);
                break;
            case 8:
                redorBombas = controle.fileiraSuperior(cod);
                break;
            default:
                redorBombas = controle.jButtonsCentro(cod);
        }

        if (redorBombas != 0) {
            switch (redorBombas) {
                case 1:
                    auxiliar.setForeground(Color.blue);
                    break;
                case 2:
                    auxiliar.setForeground(Color.yellow);
                    break;
                case 3:
                    auxiliar.setForeground(Color.red);
                    break;
                case 4:
                    auxiliar.setForeground(Color.cyan);
                    break;
                case 5:
                    auxiliar.setForeground(Color.GREEN);
                    break;
                case 6:
                    auxiliar.setForeground(Color.ORANGE);
                    break;
                case 7:
                    auxiliar.setForeground(Color.pink);
                    break;
                default:
                    auxiliar.setForeground(Color.MAGENTA);
            }

            String sc = String.valueOf(redorBombas);
            auxiliar.setText(sc);

        }
    }

    private void setButtons(JButton auxiliar, int cod) {
        int i, redorBombas,x = 0;
         int posicao;
        boolean bum = false;
        
        for(i = 0; i < codBandeiras.size();i++){
            for(int j = 0; j < bombas.size(); j++){
                if(codBandeiras.get(i).equals(bombas.get(j)))
                    x++;
            }
        }
        if(x == bombas.size() && !fimTempo){
            jLabelResultado.setForeground(Color.blue);
            jLabelResultado.setText("PARABÉNS, VOCÊ GANHOU!");
            ganhou = true;
           }
        
        
        if (bandeira) {
             for(i = 0; i < codBandeiras.size(); i++){
                if(codBandeiras.get(i).equals(cod)){// verifica se já possui uma bandeira
                    return;
                }
             }
            if (quantBandeiras > 0) {
                auxiliar.setIcon(atencao);// coloca uma bandeira
                quantBandeiras--;
                jLabelQuantBandeiras.setText("" + quantBandeiras);
                codBandeiras.add(cod);
            }
            return;
        }else{
            for(i = 0; i < codBandeiras.size(); i++){// retira a bandeira caso tenha
                if(codBandeiras.get(i).equals(cod)){
              auxiliar.setIcon(null);
              quantBandeiras++;
              jLabelQuantBandeiras.setText("" + quantBandeiras);
              codBandeiras.remove(i);
              return;

                }
        }
        
        }
        

        if (!inicio) {// chama o metodo adicionaBotoes para colocar os instancias dos botões no ArrayList
            new Thread(cronometro).start();
            adicionaBotoes();
            inicio = true;
        }

        i = 0;
        while (i < codigos.size()) {
            if (codigos.get(i).equals(cod)) {//verifica se o botão ja foi setado
                return;
            }
            i++;
        }

        codigos.add(cod);
        auxiliar = (JButton) botao.get(cod);

        auxiliar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        //  auxiliar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        for (i = 0; i < bombas.size() && !bum; i++) {
            if (bombas.get(i).equals(cod)) {
                bum = true;
            }
        }

        if (bum && !fimDeJogo || fimTempo || ganhou) {

            if (!fimTempo && !ganhou) {
                auxiliar.setBackground(Color.red);
                auxiliar.setIcon(icone);
                jLabelResultado.setText(" VOCÊ PERDEU! ");

            } else {
                if(bum){
               auxiliar.setBackground(Color.red);
               auxiliar.setIcon(icone);
                }else{
                  auxiliar.setBackground(Color.lightGray);   
            }
            }
            new Thread(abrir).start();
            fimDeJogo = true;

        } else if (!bum) {
            auxiliar.setBackground(Color.lightGray);
            posicao = 9;

            if (cod == 1) {
                posicao = 1;
            }

            for (i = 11; i <= 81; i = (i + 10)) {
                if (cod == i) {
                    posicao = 2;
                }
            }

            if (cod == 91) {
                posicao = 3;
            }

            if (cod > 91 && cod < 100) {
                posicao = 4;
            }

            if (cod == 100) {
                posicao = 5;
            }

            for (i = 20; i <= 90; i = (i + 10)) {
                if (cod == i) {
                    posicao = 6;
                }

            }

            if (cod == 10) {
                posicao = 7;
            }

            if (cod > 1 && cod < 10) {
                posicao = 8;
            }

            switch (posicao) {
                case 1:
                    redorBombas = controle.jButtonSuperiorEsquerdo(cod);
                    break;
                case 2:
                    redorBombas = controle.fileiraEsquerda(cod);
                    break;
                case 3:
                    redorBombas = controle.jButtonInferiorEsquerdo(cod);
                    break;
                case 4:
                    redorBombas = controle.fileiraInferior(cod);
                    break;
                case 5:
                    redorBombas = controle.jButtonInferiorDireito(cod);
                    break;
                case 6:
                    redorBombas = controle.fileiraDireita(cod);
                    break;
                case 7:
                    redorBombas = controle.jButtonSuperiorDireito(cod);
                    break;
                case 8:
                    redorBombas = controle.fileiraSuperior(cod);
                    break;
                default:
                    redorBombas = controle.jButtonsCentro(cod);
            }

            if (redorBombas != 0) {
                switch (redorBombas) {
                    case 1:
                        auxiliar.setForeground(Color.blue);
                        break;
                    case 2:
                        auxiliar.setForeground(Color.yellow);
                        break;
                    case 3:
                        auxiliar.setForeground(Color.red);
                        break;
                    case 4:
                        auxiliar.setForeground(Color.cyan);
                        break;
                    case 5:
                        auxiliar.setForeground(Color.GREEN);
                        break;
                    case 6:
                        auxiliar.setForeground(Color.ORANGE);
                        break;
                    case 7:
                        auxiliar.setForeground(Color.pink);
                        break;
                    default:
                        auxiliar.setForeground(Color.MAGENTA);
                }

                String sc = String.valueOf(redorBombas);
                auxiliar.setText(sc);

            } else {
                if (!fimDeJogo) {
                    openJbutton(cod, posicao);
                }

            }
        }
    }

    private void openJbutton(int cod, int posicao) {
        int x, y, i = 0;
        JButton aux;

        switch (posicao) {
            case 1:
                y = (cod + 1);
                x = (cod + 10);

                aux = (JButton) botao.get(y);
                setButtons(aux, y);

                aux = (JButton) botao.get(x);
                setButtons(aux, x);

                aux = (JButton) botao.get(x + 1);
                setButtons(aux, (x + 1));
                break;
            case 2:
                y = (cod - 10);
                x = (cod + 10);

                aux = (JButton) botao.get(y);
                setButtons(aux, y);

                aux = (JButton) botao.get(y + 1);
                setButtons(aux, (y + 1));

                aux = (JButton) botao.get(cod + 1);
                setButtons(aux, (cod + 1));

                aux = (JButton) botao.get(x);
                setButtons(aux, x);

                aux = (JButton) botao.get(x + 1);
                setButtons(aux, (x + 1));
                break;
            case 3:
                y = (cod - 10);
                x = (cod + 1);

                aux = (JButton) botao.get(y);
                setButtons(aux, y);

                aux = (JButton) botao.get(y + 1);
                setButtons(aux, (y + 1));

                aux = (JButton) botao.get(x);
                setButtons(aux, x);
                break;
            case 4:
                y = (cod - 10);
                x = cod;

                aux = (JButton) botao.get(y - 1);
                setButtons(aux, (y - 1));

                aux = (JButton) botao.get(y);
                setButtons(aux, y);

                aux = (JButton) botao.get(y + 1);
                setButtons(aux, (y + 1));

                aux = (JButton) botao.get(x - 1);
                setButtons(aux, (x - 1));

                aux = (JButton) botao.get(x + 1);
                setButtons(aux, (x + 1));
                break;
            case 5:
                y = (cod - 10);
                x = (cod - 1);

                aux = (JButton) botao.get(y - 1);
                setButtons(aux, (y - 1));

                aux = (JButton) botao.get(y);
                setButtons(aux, y);

                aux = (JButton) botao.get(x);
                setButtons(aux, x);
                break;
            case 6:
                y = (cod - 10);
                x = (cod + 10);

                aux = (JButton) botao.get(y - 1);
                setButtons(aux, (y - 1));

                aux = (JButton) botao.get(y);
                setButtons(aux, y);

                aux = (JButton) botao.get(cod - 1);
                setButtons(aux, (cod - 1));

                aux = (JButton) botao.get(x - 1);
                setButtons(aux, (x - 1));

                aux = (JButton) botao.get(x);
                setButtons(aux, x);
                break;
            case 7:
                y = (cod - 1);
                x = (cod + 10);

                aux = (JButton) botao.get(y);
                setButtons(aux, y);

                aux = (JButton) botao.get(x - 1);
                setButtons(aux, (x - 1));

                aux = (JButton) botao.get(x);
                setButtons(aux, x);
                break;
            case 8:
                y = cod;
                x = (cod + 10);

                aux = (JButton) botao.get(y - 1);
                setButtons(aux, (y - 1));

                aux = (JButton) botao.get(x - 1);
                setButtons(aux, (y - 1));
 
                aux = (JButton) botao.get(x);
                setButtons(aux, x);
     
                aux = (JButton) botao.get(x + 1);
                setButtons(aux, (x + 1));
                break;
            default:
                y = (cod - 10);
                x = (cod + 10);

                aux = (JButton) botao.get(y - 1);
                setButtons(aux, (y - 1));
    
                aux = (JButton) botao.get(y);
                setButtons(aux, y);
   
                aux = (JButton) botao.get(y + 1);
                setButtons(aux, (y + 1));
   
                aux = (JButton) botao.get(cod - 1);
                setButtons(aux, (cod - 1));
   
                aux = (JButton) botao.get(cod + 1);
                setButtons(aux, (cod + 1));
   
                aux = (JButton) botao.get(x - 1);
                setButtons(aux, (x - 1));
   
                aux = (JButton) botao.get(x);
                setButtons(aux, x);
   
                aux = (JButton) botao.get(x + 1);
                setButtons(aux, (x + 1));

        }

    }

    private void adicionaBotoes() {

        botao.add(this);
        botao.add(jButton01);
        botao.add(jButton02);
        botao.add(jButton03);
        botao.add(jButton04);
        botao.add(jButton05);
        botao.add(jButton06);
        botao.add(jButton07);
        botao.add(jButton08);
        botao.add(jButton09);
        botao.add(jButton010);
        botao.add(jButton011);
        botao.add(jButton012);
        botao.add(jButton013);
        botao.add(jButton014);
        botao.add(jButton015);
        botao.add(jButton016);
        botao.add(jButton017);
        botao.add(jButton018);
        botao.add(jButton019);
        botao.add(jButton020);
        botao.add(jButton021);

        botao.add(jButton022);
        botao.add(jButton023);
        botao.add(jButton024);
        botao.add(jButton025);
        botao.add(jButton026);
        botao.add(jButton027);
        botao.add(jButton028);
        botao.add(jButton029);
        botao.add(jButton030);
        botao.add(jButton031);
        botao.add(jButton032);
        botao.add(jButton033);
        botao.add(jButton034);
        botao.add(jButton035);
        botao.add(jButton036);
        botao.add(jButton037);
        botao.add(jButton038);
        botao.add(jButton039);
        botao.add(jButton040);
        botao.add(jButton041);
        botao.add(jButton042);

        botao.add(jButton043);
        botao.add(jButton044);
        botao.add(jButton045);
        botao.add(jButton046);
        botao.add(jButton047);
        botao.add(jButton048);
        botao.add(jButton049);
        botao.add(jButton050);
        botao.add(jButton051);
        botao.add(jButton052);
        botao.add(jButton053);
        botao.add(jButton054);
        botao.add(jButton055);
        botao.add(jButton056);
        botao.add(jButton057);
        botao.add(jButton058);
        botao.add(jButton059);
        botao.add(jButton060);

        botao.add(jButton061);
        botao.add(jButton062);
        botao.add(jButton063);
        botao.add(jButton064);
        botao.add(jButton065);
        botao.add(jButton066);
        botao.add(jButton067);
        botao.add(jButton068);
        botao.add(jButton069);
        botao.add(jButton070);
        botao.add(jButton071);
        botao.add(jButton072);
        botao.add(jButton073);
        botao.add(jButton074);
        botao.add(jButton075);
        botao.add(jButton076);
        botao.add(jButton077);
        botao.add(jButton078);

        botao.add(jButton079);
        botao.add(jButton080);
        botao.add(jButton081);
        botao.add(jButton082);
        botao.add(jButton083);
        botao.add(jButton084);
        botao.add(jButton085);
        botao.add(jButton086);
        botao.add(jButton087);
        botao.add(jButton088);
        botao.add(jButton089);
        botao.add(jButton090);
        botao.add(jButton091);
        botao.add(jButton092);
        botao.add(jButton093);
        botao.add(jButton094);
        botao.add(jButton095);
        botao.add(jButton096);
        botao.add(jButton097);
        botao.add(jButton098);
        botao.add(jButton099);
        botao.add(jButton0100);

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton01;
    private javax.swing.JButton jButton010;
    private javax.swing.JButton jButton0100;
    private javax.swing.JButton jButton011;
    private javax.swing.JButton jButton012;
    private javax.swing.JButton jButton013;
    private javax.swing.JButton jButton014;
    private javax.swing.JButton jButton015;
    private javax.swing.JButton jButton016;
    private javax.swing.JButton jButton017;
    private javax.swing.JButton jButton018;
    private javax.swing.JButton jButton019;
    private javax.swing.JButton jButton02;
    private javax.swing.JButton jButton020;
    private javax.swing.JButton jButton021;
    private javax.swing.JButton jButton022;
    private javax.swing.JButton jButton023;
    private javax.swing.JButton jButton024;
    private javax.swing.JButton jButton025;
    private javax.swing.JButton jButton026;
    private javax.swing.JButton jButton027;
    private javax.swing.JButton jButton028;
    private javax.swing.JButton jButton029;
    private javax.swing.JButton jButton03;
    private javax.swing.JButton jButton030;
    private javax.swing.JButton jButton031;
    private javax.swing.JButton jButton032;
    private javax.swing.JButton jButton033;
    private javax.swing.JButton jButton034;
    private javax.swing.JButton jButton035;
    private javax.swing.JButton jButton036;
    private javax.swing.JButton jButton037;
    private javax.swing.JButton jButton038;
    private javax.swing.JButton jButton039;
    private javax.swing.JButton jButton04;
    private javax.swing.JButton jButton040;
    private javax.swing.JButton jButton041;
    private javax.swing.JButton jButton042;
    private javax.swing.JButton jButton043;
    private javax.swing.JButton jButton044;
    private javax.swing.JButton jButton045;
    private javax.swing.JButton jButton046;
    private javax.swing.JButton jButton047;
    private javax.swing.JButton jButton048;
    private javax.swing.JButton jButton049;
    private javax.swing.JButton jButton05;
    private javax.swing.JButton jButton050;
    private javax.swing.JButton jButton051;
    private javax.swing.JButton jButton052;
    private javax.swing.JButton jButton053;
    private javax.swing.JButton jButton054;
    private javax.swing.JButton jButton055;
    private javax.swing.JButton jButton056;
    private javax.swing.JButton jButton057;
    private javax.swing.JButton jButton058;
    private javax.swing.JButton jButton059;
    private javax.swing.JButton jButton06;
    private javax.swing.JButton jButton060;
    private javax.swing.JButton jButton061;
    private javax.swing.JButton jButton062;
    private javax.swing.JButton jButton063;
    private javax.swing.JButton jButton064;
    private javax.swing.JButton jButton065;
    private javax.swing.JButton jButton066;
    private javax.swing.JButton jButton067;
    private javax.swing.JButton jButton068;
    private javax.swing.JButton jButton069;
    private javax.swing.JButton jButton07;
    private javax.swing.JButton jButton070;
    private javax.swing.JButton jButton071;
    private javax.swing.JButton jButton072;
    private javax.swing.JButton jButton073;
    private javax.swing.JButton jButton074;
    private javax.swing.JButton jButton075;
    private javax.swing.JButton jButton076;
    private javax.swing.JButton jButton077;
    private javax.swing.JButton jButton078;
    private javax.swing.JButton jButton079;
    private javax.swing.JButton jButton08;
    private javax.swing.JButton jButton080;
    private javax.swing.JButton jButton081;
    private javax.swing.JButton jButton082;
    private javax.swing.JButton jButton083;
    private javax.swing.JButton jButton084;
    private javax.swing.JButton jButton085;
    private javax.swing.JButton jButton086;
    private javax.swing.JButton jButton087;
    private javax.swing.JButton jButton088;
    private javax.swing.JButton jButton089;
    private javax.swing.JButton jButton09;
    private javax.swing.JButton jButton090;
    private javax.swing.JButton jButton091;
    private javax.swing.JButton jButton092;
    private javax.swing.JButton jButton093;
    private javax.swing.JButton jButton094;
    private javax.swing.JButton jButton095;
    private javax.swing.JButton jButton096;
    private javax.swing.JButton jButton097;
    private javax.swing.JButton jButton098;
    private javax.swing.JButton jButton099;
    private javax.swing.JButton jButtonAtencao;
    private javax.swing.JButton jButtonConfiguracoes;
    private javax.swing.JButton jButtonReiniciar;
    private javax.swing.JButton jButtonSair;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelCronometro;
    private javax.swing.JLabel jLabelQuantBandeiras;
    private javax.swing.JLabel jLabelResultado;
    private javax.swing.JLabel jLabelTextoTempo;
    // End of variables declaration//GEN-END:variables
}
